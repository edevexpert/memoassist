﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MemoAssist
{
    using Forms;

    public partial class MemoAssistantRibbon
    {
        private void btnMemoAssistant_Click(object sender, RibbonControlEventArgs e)
        {
            var mailBody = string.Empty;
            var subjects = new List<string>();
            var extractedMails = new List<string>();

            try
            {
                var mails = Globals.ThisAddIn.Application.ActiveExplorer().Selection;

                if (mails != null && mails.Count != 0)
                {
                    foreach (object mailObject in mails)
                    {
                        if (mailObject is Outlook.MailItem mailItem)
                        {
                            var mail = (Outlook.MailItem)mailObject;
                            MemoAssist.ExtractMailAddresses(mail, extractedMails);

                            mailBody = mail.Body;

                            subjects.Add(mail.Subject);
                        }
                    }

                    var task = Task.Run(() => RunMemoAssistant(extractedMails, subjects, mailBody));
                }
            }
            catch { }
        }

        private void RunMemoAssistant(List<string> extractedMails, List<string> subjects, string mailBody)
        {
            var memoAssistant = new MemoAssistant(extractedMails, subjects, mailBody);
            memoAssistant.ShowDialog();
        }
    }
}
