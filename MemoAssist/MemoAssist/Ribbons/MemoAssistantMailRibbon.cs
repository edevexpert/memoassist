﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MemoAssist
{
    using Forms;

    public partial class MemoAssistantMailRibbon
    {
        private void btnMemoAssistant_Click(object sender, RibbonControlEventArgs e)
        {
            var mailBody = string.Empty;
            var subjects = new List<string>();
            var extractedMails = new List<string>();

            try
            {

                var inspector = e.Control.Context as Outlook.Inspector;

                if (inspector != null)
                {
                    if (inspector.CurrentItem is Outlook.MailItem)
                    {
                        var mail = inspector.CurrentItem as Outlook.MailItem;

                        if (mail != null)
                        {
                            MemoAssist.ExtractMailAddresses(mail, extractedMails);

                            mailBody = mail.Body;

                            subjects.Add(mail.Subject);

                            var task = Task.Run(() => RunMemoAssistant(extractedMails, subjects, mailBody));
                        }
                    }
                }
            }
            catch { }
        }

        private void RunMemoAssistant(List<string> extractedMails, List<string> subjects, string mailBody)
        {
            var memoAssistant = new MemoAssistant(extractedMails, subjects, mailBody);
            memoAssistant.ShowDialog();
        }
    }
}
