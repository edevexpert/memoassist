﻿namespace MemoAssist
{
    partial class MemoAssistantRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public MemoAssistantRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemoAssistantRibbon));
            this.tabMemoAssistant = this.Factory.CreateRibbonTab();
            this.groupMemoAssistant = this.Factory.CreateRibbonGroup();
            this.btnMemoAssistant = this.Factory.CreateRibbonButton();
            this.tabMemoAssistant.SuspendLayout();
            this.groupMemoAssistant.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMemoAssistant
            // 
            this.tabMemoAssistant.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabMemoAssistant.ControlId.OfficeId = "TabMail";
            this.tabMemoAssistant.Groups.Add(this.groupMemoAssistant);
            this.tabMemoAssistant.Label = "TabMail";
            this.tabMemoAssistant.Name = "tabMemoAssistant";
            // 
            // groupMemoAssistant
            // 
            this.groupMemoAssistant.Items.Add(this.btnMemoAssistant);
            this.groupMemoAssistant.Label = "Memo Assistant";
            this.groupMemoAssistant.Name = "groupMemoAssistant";
            // 
            // btnMemoAssistant
            // 
            this.btnMemoAssistant.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnMemoAssistant.Image = ((System.Drawing.Image)(resources.GetObject("btnMemoAssistant.Image")));
            this.btnMemoAssistant.Label = "Memo Assistant";
            this.btnMemoAssistant.Name = "btnMemoAssistant";
            this.btnMemoAssistant.ShowImage = true;
            this.btnMemoAssistant.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnMemoAssistant_Click);
            // 
            // MemoAssistantRibbon
            // 
            this.Name = "MemoAssistantRibbon";
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tabMemoAssistant);
            this.tabMemoAssistant.ResumeLayout(false);
            this.tabMemoAssistant.PerformLayout();
            this.groupMemoAssistant.ResumeLayout(false);
            this.groupMemoAssistant.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabMemoAssistant;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupMemoAssistant;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnMemoAssistant;
    }

    partial class ThisRibbonCollection
    {
        internal MemoAssistantRibbon MemoAssistantRibbon
        {
            get { return this.GetRibbon<MemoAssistantRibbon>(); }
        }
    }
}
