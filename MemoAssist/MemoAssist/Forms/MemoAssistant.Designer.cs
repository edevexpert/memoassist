﻿namespace MemoAssist.Forms
{
    partial class MemoAssistant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemoAssistant));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblEmailsExtractedFromMessage = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnBodyAdd = new System.Windows.Forms.Button();
            this.btnBodyDelete = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.pnlHelp = new System.Windows.Forms.Panel();
            this.btnHelp = new System.Windows.Forms.Button();
            this.lblMemoAssistant = new System.Windows.Forms.Label();
            this.pctBox = new System.Windows.Forms.PictureBox();
            this.lblHelpFooter = new System.Windows.Forms.Label();
            this.lblMemoBody = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.lblLengthOfMeeting = new System.Windows.Forms.Label();
            this.btnConfigureMeetingLength = new System.Windows.Forms.Button();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.lblMeetingUrgency = new System.Windows.Forms.Label();
            this.btnConfigureMeetingUrgency = new System.Windows.Forms.Button();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.btnConfigureDeliveryTo = new System.Windows.Forms.Button();
            this.lblDeliveryTo = new System.Windows.Forms.Label();
            this.lblSubjectOfEmail = new System.Windows.Forms.Label();
            this.btnOriginal = new System.Windows.Forms.Button();
            this.btnConfigureSubject = new System.Windows.Forms.Button();
            this.btnSelectAllDeliveryAddresses = new System.Windows.Forms.Button();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.lblMailStatus = new System.Windows.Forms.Label();
            this.pctStatus = new System.Windows.Forms.PictureBox();
            this.lblMailStatusFailed = new System.Windows.Forms.Label();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.btnConfigureLocation = new System.Windows.Forms.Button();
            this.lblLocation = new System.Windows.Forms.Label();
            this.btnBodyEdit = new System.Windows.Forms.Button();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.lblDeliveryCc = new System.Windows.Forms.Label();
            this.btnSelectAllDeliveryCc = new System.Windows.Forms.Button();
            this.btnConfigureDeliveryCc = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.pnlHelp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.pnlStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.SuspendLayout();
            // 
            // lblEmailsExtractedFromMessage
            // 
            this.lblEmailsExtractedFromMessage.AutoSize = true;
            this.lblEmailsExtractedFromMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailsExtractedFromMessage.Location = new System.Drawing.Point(33, 40);
            this.lblEmailsExtractedFromMessage.Name = "lblEmailsExtractedFromMessage";
            this.lblEmailsExtractedFromMessage.Size = new System.Drawing.Size(160, 13);
            this.lblEmailsExtractedFromMessage.TabIndex = 0;
            this.lblEmailsExtractedFromMessage.Text = "Emails Extracted From Message:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView1.Location = new System.Drawing.Point(36, 57);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 1;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSelectAll.FlatAppearance.BorderSize = 0;
            this.btnSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectAll.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSelectAll.Location = new System.Drawing.Point(36, 209);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(0);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAll.TabIndex = 2;
            this.btnSelectAll.Text = "Select All";
            this.btnSelectAll.UseVisualStyleBackColor = false;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAdd.Location = new System.Drawing.Point(113, 209);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Configure";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBodyAdd
            // 
            this.btnBodyAdd.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBodyAdd.FlatAppearance.BorderSize = 0;
            this.btnBodyAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBodyAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBodyAdd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBodyAdd.Location = new System.Drawing.Point(36, 670);
            this.btnBodyAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnBodyAdd.Name = "btnBodyAdd";
            this.btnBodyAdd.Size = new System.Drawing.Size(75, 23);
            this.btnBodyAdd.TabIndex = 5;
            this.btnBodyAdd.Text = "Add";
            this.btnBodyAdd.UseVisualStyleBackColor = false;
            this.btnBodyAdd.Click += new System.EventHandler(this.btnBodyAdd_Click);
            // 
            // btnBodyDelete
            // 
            this.btnBodyDelete.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBodyDelete.FlatAppearance.BorderSize = 0;
            this.btnBodyDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBodyDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBodyDelete.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBodyDelete.Location = new System.Drawing.Point(190, 670);
            this.btnBodyDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnBodyDelete.Name = "btnBodyDelete";
            this.btnBodyDelete.Size = new System.Drawing.Size(75, 23);
            this.btnBodyDelete.TabIndex = 6;
            this.btnBodyDelete.Text = "Delete";
            this.btnBodyDelete.UseVisualStyleBackColor = false;
            this.btnBodyDelete.Click += new System.EventHandler(this.btnBodyDelete_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.BackColor = System.Drawing.Color.Gold;
            this.btnPreview.FlatAppearance.BorderSize = 0;
            this.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreview.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreview.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPreview.Location = new System.Drawing.Point(964, 480);
            this.btnPreview.Margin = new System.Windows.Forms.Padding(2);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(140, 32);
            this.btnPreview.TabIndex = 7;
            this.btnPreview.Text = "PREVIEW EMAIL";
            this.btnPreview.UseVisualStyleBackColor = false;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.Red;
            this.btnSend.FlatAppearance.BorderSize = 0;
            this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSend.Location = new System.Drawing.Point(964, 514);
            this.btnSend.Margin = new System.Windows.Forms.Padding(2);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(140, 32);
            this.btnSend.TabIndex = 8;
            this.btnSend.Text = "SEND EMAIL";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // pnlHelp
            // 
            this.pnlHelp.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlHelp.Controls.Add(this.btnHelp);
            this.pnlHelp.Controls.Add(this.lblMemoAssistant);
            this.pnlHelp.Controls.Add(this.pctBox);
            this.pnlHelp.Controls.Add(this.lblHelpFooter);
            this.pnlHelp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pnlHelp.Location = new System.Drawing.Point(877, 57);
            this.pnlHelp.Margin = new System.Windows.Forms.Padding(0);
            this.pnlHelp.Name = "pnlHelp";
            this.pnlHelp.Size = new System.Drawing.Size(227, 175);
            this.pnlHelp.TabIndex = 9;
            // 
            // btnHelp
            // 
            this.btnHelp.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnHelp.FlatAppearance.BorderSize = 0;
            this.btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHelp.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnHelp.Location = new System.Drawing.Point(122, 97);
            this.btnHelp.Margin = new System.Windows.Forms.Padding(2);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(80, 41);
            this.btnHelp.TabIndex = 3;
            this.btnHelp.Text = "Help";
            this.btnHelp.UseVisualStyleBackColor = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // lblMemoAssistant
            // 
            this.lblMemoAssistant.AutoSize = true;
            this.lblMemoAssistant.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemoAssistant.Location = new System.Drawing.Point(99, 17);
            this.lblMemoAssistant.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMemoAssistant.Name = "lblMemoAssistant";
            this.lblMemoAssistant.Size = new System.Drawing.Size(117, 58);
            this.lblMemoAssistant.TabIndex = 2;
            this.lblMemoAssistant.Text = "Memo \r\nAssistant";
            this.lblMemoAssistant.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pctBox
            // 
            this.pctBox.Image = ((System.Drawing.Image)(resources.GetObject("pctBox.Image")));
            this.pctBox.Location = new System.Drawing.Point(10, 17);
            this.pctBox.Margin = new System.Windows.Forms.Padding(0);
            this.pctBox.Name = "pctBox";
            this.pctBox.Size = new System.Drawing.Size(89, 106);
            this.pctBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBox.TabIndex = 1;
            this.pctBox.TabStop = false;
            // 
            // lblHelpFooter
            // 
            this.lblHelpFooter.AutoSize = true;
            this.lblHelpFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelpFooter.Location = new System.Drawing.Point(11, 152);
            this.lblHelpFooter.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHelpFooter.Name = "lblHelpFooter";
            this.lblHelpFooter.Size = new System.Drawing.Size(190, 13);
            this.lblHelpFooter.TabIndex = 0;
            this.lblHelpFooter.Text = "@ 2018 By ipCapital  Group, Inc";
            // 
            // lblMemoBody
            // 
            this.lblMemoBody.AutoSize = true;
            this.lblMemoBody.Location = new System.Drawing.Point(33, 463);
            this.lblMemoBody.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMemoBody.Name = "lblMemoBody";
            this.lblMemoBody.Size = new System.Drawing.Size(66, 13);
            this.lblMemoBody.TabIndex = 10;
            this.lblMemoBody.Text = "Memo Body:";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.ColumnHeadersVisible = false;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView2.Location = new System.Drawing.Point(36, 480);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.White;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(926, 188);
            this.dataGridView2.TabIndex = 11;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToOrderColumns = true;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.ColumnHeadersVisible = false;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridView3.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView3.Location = new System.Drawing.Point(281, 57);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(170, 150);
            this.dataGridView3.TabIndex = 12;
            // 
            // lblLengthOfMeeting
            // 
            this.lblLengthOfMeeting.AutoSize = true;
            this.lblLengthOfMeeting.Location = new System.Drawing.Point(278, 40);
            this.lblLengthOfMeeting.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLengthOfMeeting.Name = "lblLengthOfMeeting";
            this.lblLengthOfMeeting.Size = new System.Drawing.Size(96, 13);
            this.lblLengthOfMeeting.TabIndex = 13;
            this.lblLengthOfMeeting.Text = "Length of Meeting:";
            // 
            // btnConfigureMeetingLength
            // 
            this.btnConfigureMeetingLength.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnConfigureMeetingLength.FlatAppearance.BorderSize = 0;
            this.btnConfigureMeetingLength.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigureMeetingLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfigureMeetingLength.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConfigureMeetingLength.Location = new System.Drawing.Point(281, 209);
            this.btnConfigureMeetingLength.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfigureMeetingLength.Name = "btnConfigureMeetingLength";
            this.btnConfigureMeetingLength.Size = new System.Drawing.Size(75, 23);
            this.btnConfigureMeetingLength.TabIndex = 14;
            this.btnConfigureMeetingLength.Text = "Configure";
            this.btnConfigureMeetingLength.UseVisualStyleBackColor = false;
            this.btnConfigureMeetingLength.Click += new System.EventHandler(this.btnConfigureMeetingLength_Click);
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.AllowUserToOrderColumns = true;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView4.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.ColumnHeadersVisible = false;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView4.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridView4.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView4.Location = new System.Drawing.Point(456, 57);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowHeadersVisible = false;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(170, 150);
            this.dataGridView4.TabIndex = 15;
            // 
            // lblMeetingUrgency
            // 
            this.lblMeetingUrgency.AutoSize = true;
            this.lblMeetingUrgency.Location = new System.Drawing.Point(453, 40);
            this.lblMeetingUrgency.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMeetingUrgency.Name = "lblMeetingUrgency";
            this.lblMeetingUrgency.Size = new System.Drawing.Size(91, 13);
            this.lblMeetingUrgency.TabIndex = 16;
            this.lblMeetingUrgency.Text = "Meeting Urgency:";
            // 
            // btnConfigureMeetingUrgency
            // 
            this.btnConfigureMeetingUrgency.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnConfigureMeetingUrgency.FlatAppearance.BorderSize = 0;
            this.btnConfigureMeetingUrgency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigureMeetingUrgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfigureMeetingUrgency.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConfigureMeetingUrgency.Location = new System.Drawing.Point(456, 209);
            this.btnConfigureMeetingUrgency.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfigureMeetingUrgency.Name = "btnConfigureMeetingUrgency";
            this.btnConfigureMeetingUrgency.Size = new System.Drawing.Size(75, 23);
            this.btnConfigureMeetingUrgency.TabIndex = 17;
            this.btnConfigureMeetingUrgency.Text = "Configure";
            this.btnConfigureMeetingUrgency.UseVisualStyleBackColor = false;
            this.btnConfigureMeetingUrgency.Click += new System.EventHandler(this.btnConfigureMeetingUrgency_Click);
            // 
            // dataGridView5
            // 
            this.dataGridView5.AllowUserToAddRows = false;
            this.dataGridView5.AllowUserToDeleteRows = false;
            this.dataGridView5.AllowUserToOrderColumns = true;
            this.dataGridView5.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView5.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.ColumnHeadersVisible = false;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView5.DefaultCellStyle = dataGridViewCellStyle37;
            this.dataGridView5.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView5.Location = new System.Drawing.Point(36, 269);
            this.dataGridView5.MultiSelect = false;
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.RowHeadersVisible = false;
            this.dataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView5.Size = new System.Drawing.Size(340, 150);
            this.dataGridView5.TabIndex = 18;
            // 
            // dataGridView6
            // 
            this.dataGridView6.AllowUserToAddRows = false;
            this.dataGridView6.AllowUserToDeleteRows = false;
            this.dataGridView6.AllowUserToOrderColumns = true;
            this.dataGridView6.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView6.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView6.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.ColumnHeadersVisible = false;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView6.DefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridView6.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView6.Location = new System.Drawing.Point(400, 269);
            this.dataGridView6.MultiSelect = false;
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.RowHeadersVisible = false;
            this.dataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView6.Size = new System.Drawing.Size(340, 150);
            this.dataGridView6.TabIndex = 19;
            // 
            // btnConfigureDeliveryTo
            // 
            this.btnConfigureDeliveryTo.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnConfigureDeliveryTo.FlatAppearance.BorderSize = 0;
            this.btnConfigureDeliveryTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigureDeliveryTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfigureDeliveryTo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConfigureDeliveryTo.Location = new System.Drawing.Point(113, 421);
            this.btnConfigureDeliveryTo.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfigureDeliveryTo.Name = "btnConfigureDeliveryTo";
            this.btnConfigureDeliveryTo.Size = new System.Drawing.Size(75, 23);
            this.btnConfigureDeliveryTo.TabIndex = 20;
            this.btnConfigureDeliveryTo.Text = "Configure";
            this.btnConfigureDeliveryTo.UseVisualStyleBackColor = false;
            this.btnConfigureDeliveryTo.Click += new System.EventHandler(this.btnConfigureDeliveryTo_Click);
            // 
            // lblDeliveryTo
            // 
            this.lblDeliveryTo.AutoSize = true;
            this.lblDeliveryTo.Location = new System.Drawing.Point(33, 252);
            this.lblDeliveryTo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDeliveryTo.Name = "lblDeliveryTo";
            this.lblDeliveryTo.Size = new System.Drawing.Size(143, 13);
            this.lblDeliveryTo.TabIndex = 21;
            this.lblDeliveryTo.Text = "Memo Template Delivery To:";
            // 
            // lblSubjectOfEmail
            // 
            this.lblSubjectOfEmail.AutoSize = true;
            this.lblSubjectOfEmail.Location = new System.Drawing.Point(397, 252);
            this.lblSubjectOfEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSubjectOfEmail.Name = "lblSubjectOfEmail";
            this.lblSubjectOfEmail.Size = new System.Drawing.Size(86, 13);
            this.lblSubjectOfEmail.TabIndex = 22;
            this.lblSubjectOfEmail.Text = "Subject of Email:";
            // 
            // btnOriginal
            // 
            this.btnOriginal.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnOriginal.FlatAppearance.BorderSize = 0;
            this.btnOriginal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOriginal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOriginal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOriginal.Location = new System.Drawing.Point(406, 421);
            this.btnOriginal.Margin = new System.Windows.Forms.Padding(2);
            this.btnOriginal.Name = "btnOriginal";
            this.btnOriginal.Size = new System.Drawing.Size(75, 23);
            this.btnOriginal.TabIndex = 23;
            this.btnOriginal.Text = "Original";
            this.btnOriginal.UseVisualStyleBackColor = false;
            this.btnOriginal.Click += new System.EventHandler(this.btnOriginal_Click);
            // 
            // btnConfigureSubject
            // 
            this.btnConfigureSubject.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnConfigureSubject.FlatAppearance.BorderSize = 0;
            this.btnConfigureSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigureSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfigureSubject.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConfigureSubject.Location = new System.Drawing.Point(400, 421);
            this.btnConfigureSubject.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfigureSubject.Name = "btnConfigureSubject";
            this.btnConfigureSubject.Size = new System.Drawing.Size(75, 23);
            this.btnConfigureSubject.TabIndex = 24;
            this.btnConfigureSubject.Text = "Configure";
            this.btnConfigureSubject.UseVisualStyleBackColor = false;
            this.btnConfigureSubject.Click += new System.EventHandler(this.btnConfigureSubject_Click);
            // 
            // btnSelectAllDeliveryAddresses
            // 
            this.btnSelectAllDeliveryAddresses.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSelectAllDeliveryAddresses.FlatAppearance.BorderSize = 0;
            this.btnSelectAllDeliveryAddresses.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectAllDeliveryAddresses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectAllDeliveryAddresses.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSelectAllDeliveryAddresses.Location = new System.Drawing.Point(36, 421);
            this.btnSelectAllDeliveryAddresses.Margin = new System.Windows.Forms.Padding(0);
            this.btnSelectAllDeliveryAddresses.Name = "btnSelectAllDeliveryAddresses";
            this.btnSelectAllDeliveryAddresses.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAllDeliveryAddresses.TabIndex = 25;
            this.btnSelectAllDeliveryAddresses.Text = "Select All";
            this.btnSelectAllDeliveryAddresses.UseVisualStyleBackColor = false;
            this.btnSelectAllDeliveryAddresses.Click += new System.EventHandler(this.btnSelectAllDeliveryAddresses_Click);
            // 
            // pnlStatus
            // 
            this.pnlStatus.Controls.Add(this.lblMailStatus);
            this.pnlStatus.Controls.Add(this.pctStatus);
            this.pnlStatus.Location = new System.Drawing.Point(965, 548);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(139, 120);
            this.pnlStatus.TabIndex = 26;
            this.pnlStatus.Visible = false;
            // 
            // lblMailStatus
            // 
            this.lblMailStatus.AutoSize = true;
            this.lblMailStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMailStatus.ForeColor = System.Drawing.Color.Green;
            this.lblMailStatus.Location = new System.Drawing.Point(5, 95);
            this.lblMailStatus.Name = "lblMailStatus";
            this.lblMailStatus.Size = new System.Drawing.Size(135, 13);
            this.lblMailStatus.TabIndex = 1;
            this.lblMailStatus.Text = "Mail sent successfully!";
            // 
            // pctStatus
            // 
            this.pctStatus.Image = ((System.Drawing.Image)(resources.GetObject("pctStatus.Image")));
            this.pctStatus.Location = new System.Drawing.Point(5, 3);
            this.pctStatus.Name = "pctStatus";
            this.pctStatus.Size = new System.Drawing.Size(134, 86);
            this.pctStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctStatus.TabIndex = 0;
            this.pctStatus.TabStop = false;
            // 
            // lblMailStatusFailed
            // 
            this.lblMailStatusFailed.AutoSize = true;
            this.lblMailStatusFailed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.95F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMailStatusFailed.ForeColor = System.Drawing.Color.Red;
            this.lblMailStatusFailed.Location = new System.Drawing.Point(978, 593);
            this.lblMailStatusFailed.Name = "lblMailStatusFailed";
            this.lblMailStatusFailed.Size = new System.Drawing.Size(115, 17);
            this.lblMailStatusFailed.TabIndex = 27;
            this.lblMailStatusFailed.Text = "Email not sent!";
            this.lblMailStatusFailed.UseWaitCursor = true;
            this.lblMailStatusFailed.Visible = false;
            // 
            // dataGridView7
            // 
            this.dataGridView7.AllowUserToAddRows = false;
            this.dataGridView7.AllowUserToDeleteRows = false;
            this.dataGridView7.AllowUserToOrderColumns = true;
            this.dataGridView7.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView7.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView7.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.ColumnHeadersVisible = false;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView7.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridView7.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView7.Location = new System.Drawing.Point(631, 57);
            this.dataGridView7.MultiSelect = false;
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.RowHeadersVisible = false;
            this.dataGridView7.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView7.Size = new System.Drawing.Size(240, 150);
            this.dataGridView7.TabIndex = 28;
            // 
            // btnConfigureLocation
            // 
            this.btnConfigureLocation.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnConfigureLocation.FlatAppearance.BorderSize = 0;
            this.btnConfigureLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigureLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfigureLocation.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConfigureLocation.Location = new System.Drawing.Point(631, 209);
            this.btnConfigureLocation.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfigureLocation.Name = "btnConfigureLocation";
            this.btnConfigureLocation.Size = new System.Drawing.Size(75, 23);
            this.btnConfigureLocation.TabIndex = 29;
            this.btnConfigureLocation.Text = "Configure";
            this.btnConfigureLocation.UseVisualStyleBackColor = false;
            this.btnConfigureLocation.Click += new System.EventHandler(this.btnConfigureLocation_Click);
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(628, 40);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(104, 13);
            this.lblLocation.TabIndex = 30;
            this.lblLocation.Text = "Location of Meeting:";
            // 
            // btnBodyEdit
            // 
            this.btnBodyEdit.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBodyEdit.FlatAppearance.BorderSize = 0;
            this.btnBodyEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBodyEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBodyEdit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBodyEdit.Location = new System.Drawing.Point(113, 670);
            this.btnBodyEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnBodyEdit.Name = "btnBodyEdit";
            this.btnBodyEdit.Size = new System.Drawing.Size(75, 23);
            this.btnBodyEdit.TabIndex = 31;
            this.btnBodyEdit.Text = "Edit";
            this.btnBodyEdit.UseVisualStyleBackColor = false;
            this.btnBodyEdit.Click += new System.EventHandler(this.btnBodyEdit_Click);
            // 
            // dataGridView8
            // 
            this.dataGridView8.AllowUserToAddRows = false;
            this.dataGridView8.AllowUserToDeleteRows = false;
            this.dataGridView8.AllowUserToOrderColumns = true;
            this.dataGridView8.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView8.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView8.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.ColumnHeadersVisible = false;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView8.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridView8.GridColor = System.Drawing.SystemColors.MenuHighlight;
            this.dataGridView8.Location = new System.Drawing.Point(764, 269);
            this.dataGridView8.MultiSelect = false;
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.RowHeadersVisible = false;
            this.dataGridView8.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView8.Size = new System.Drawing.Size(340, 150);
            this.dataGridView8.TabIndex = 32;
            // 
            // lblDeliveryCc
            // 
            this.lblDeliveryCc.AutoSize = true;
            this.lblDeliveryCc.Location = new System.Drawing.Point(761, 252);
            this.lblDeliveryCc.Name = "lblDeliveryCc";
            this.lblDeliveryCc.Size = new System.Drawing.Size(146, 13);
            this.lblDeliveryCc.TabIndex = 33;
            this.lblDeliveryCc.Text = "Memo Template Delivery Cc: ";
            // 
            // btnSelectAllDeliveryCc
            // 
            this.btnSelectAllDeliveryCc.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSelectAllDeliveryCc.FlatAppearance.BorderSize = 0;
            this.btnSelectAllDeliveryCc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectAllDeliveryCc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectAllDeliveryCc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSelectAllDeliveryCc.Location = new System.Drawing.Point(764, 421);
            this.btnSelectAllDeliveryCc.Margin = new System.Windows.Forms.Padding(0);
            this.btnSelectAllDeliveryCc.Name = "btnSelectAllDeliveryCc";
            this.btnSelectAllDeliveryCc.Size = new System.Drawing.Size(75, 23);
            this.btnSelectAllDeliveryCc.TabIndex = 34;
            this.btnSelectAllDeliveryCc.Text = "Select All";
            this.btnSelectAllDeliveryCc.UseVisualStyleBackColor = false;
            this.btnSelectAllDeliveryCc.Click += new System.EventHandler(this.btnSelectAllDeliveryCc_Click);
            // 
            // btnConfigureDeliveryCc
            // 
            this.btnConfigureDeliveryCc.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnConfigureDeliveryCc.FlatAppearance.BorderSize = 0;
            this.btnConfigureDeliveryCc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigureDeliveryCc.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfigureDeliveryCc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConfigureDeliveryCc.Location = new System.Drawing.Point(841, 421);
            this.btnConfigureDeliveryCc.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfigureDeliveryCc.Name = "btnConfigureDeliveryCc";
            this.btnConfigureDeliveryCc.Size = new System.Drawing.Size(75, 23);
            this.btnConfigureDeliveryCc.TabIndex = 35;
            this.btnConfigureDeliveryCc.Text = "Configure";
            this.btnConfigureDeliveryCc.UseVisualStyleBackColor = false;
            this.btnConfigureDeliveryCc.Click += new System.EventHandler(this.btnConfigureDeliveryCc_Click);
            // 
            // MemoAssistant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 729);
            this.Controls.Add(this.btnConfigureDeliveryCc);
            this.Controls.Add(this.btnSelectAllDeliveryCc);
            this.Controls.Add(this.lblDeliveryCc);
            this.Controls.Add(this.dataGridView8);
            this.Controls.Add(this.btnBodyEdit);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.btnConfigureLocation);
            this.Controls.Add(this.dataGridView7);
            this.Controls.Add(this.lblMailStatusFailed);
            this.Controls.Add(this.pnlStatus);
            this.Controls.Add(this.btnSelectAllDeliveryAddresses);
            this.Controls.Add(this.btnConfigureSubject);
            this.Controls.Add(this.btnOriginal);
            this.Controls.Add(this.lblSubjectOfEmail);
            this.Controls.Add(this.lblDeliveryTo);
            this.Controls.Add(this.btnConfigureDeliveryTo);
            this.Controls.Add(this.dataGridView6);
            this.Controls.Add(this.dataGridView5);
            this.Controls.Add(this.btnConfigureMeetingUrgency);
            this.Controls.Add(this.lblMeetingUrgency);
            this.Controls.Add(this.dataGridView4);
            this.Controls.Add(this.btnConfigureMeetingLength);
            this.Controls.Add(this.lblLengthOfMeeting);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.lblMemoBody);
            this.Controls.Add(this.pnlHelp);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnBodyDelete);
            this.Controls.Add(this.btnBodyAdd);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblEmailsExtractedFromMessage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MemoAssistant";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Memo Assistant";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MemoAssistant_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.pnlHelp.ResumeLayout(false);
            this.pnlHelp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.pnlStatus.ResumeLayout(false);
            this.pnlStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEmailsExtractedFromMessage;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnBodyAdd;
        private System.Windows.Forms.Button btnBodyDelete;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Panel pnlHelp;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.Label lblMemoAssistant;
        private System.Windows.Forms.PictureBox pctBox;
        private System.Windows.Forms.Label lblHelpFooter;
        private System.Windows.Forms.Label lblMemoBody;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label lblLengthOfMeeting;
        private System.Windows.Forms.Button btnConfigureMeetingLength;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Label lblMeetingUrgency;
        private System.Windows.Forms.Button btnConfigureMeetingUrgency;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Button btnConfigureDeliveryTo;
        private System.Windows.Forms.Label lblDeliveryTo;
        private System.Windows.Forms.Label lblSubjectOfEmail;
        private System.Windows.Forms.Button btnOriginal;
        private System.Windows.Forms.Button btnConfigureSubject;
        private System.Windows.Forms.Button btnSelectAllDeliveryAddresses;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.PictureBox pctStatus;
        private System.Windows.Forms.Label lblMailStatus;
        private System.Windows.Forms.Label lblMailStatusFailed;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.Button btnConfigureLocation;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Button btnBodyEdit;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.Label lblDeliveryCc;
        private System.Windows.Forms.Button btnSelectAllDeliveryCc;
        private System.Windows.Forms.Button btnConfigureDeliveryCc;
    }
}