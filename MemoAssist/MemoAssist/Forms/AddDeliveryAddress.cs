﻿using System;
using System.Windows.Forms;

namespace MemoAssist.Forms
{
    public partial class AddDeliveryAddress : Form
    {
        public string Address
        {
            get
            {
                return txtAddress.Text;   
            }
        }

        public string ShortForm
        {
            get
            {
                return txtShortform.Text;
            }
        }

        public AddDeliveryAddress()
        {
            InitializeComponent();

            btnAdd.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
