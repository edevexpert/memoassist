﻿using System;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MemoAssist.Forms
{
    public partial class MemoAssistant : Form
    {
        private string mailBody = string.Empty;

        private List<string> extractedMails;
        private List<string> subjects;

        private BindingSource bsExtractedMails = new BindingSource();
        private BindingSource bsMemoBody = new BindingSource();

        private PersistentModel persistenceModel = default(PersistentModel);

        public MemoAssistant(List<string> extractedMails, List<string> subjects, string mailBody)
        {
            InitializeComponent();

            this.extractedMails = extractedMails;
            this.subjects = subjects;
            this.mailBody = mailBody;

            persistenceModel = MemoAssist.Deserialize();

            Load += new EventHandler(FormGrid);
        }

        private void FormGrid(object sender, EventArgs e)
        {
            foreach (var mail in extractedMails)
            {
                bsExtractedMails.Add(new Item(mail, false, false, false));
            }
            foreach (var mail in persistenceModel.ExtractedMails)
            {
                bsExtractedMails.Add(new Item(mail, false, true, false));
            }

            // Emails Extracted
            MemoAssist.SetDataGridView(dataGridView1, bsExtractedMails);

            // Memo Body
            foreach (var mail in persistenceModel.MemoBody)
            {
                bsMemoBody.Add(new Item(mail, false, true, false));
            }

            if (bsMemoBody.Count != 0)
            {
                MemoAssist.SetDataGridView(dataGridView2, bsMemoBody, true);
            }

            InitLengthOfMeeting();
            InitMeetingUrgency();
            InitDeliveryTo();
            InitSubject();
            InitLocation();
            InitDeliveryCc();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.Cells[1].Value = true;
            }
        }

        private void btnSelectAllDeliveryAddresses_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView5.Rows)
            {
                row.Cells[1].Value = true;
            }
        }

        private void btnSelectAllDeliveryCc_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView8.Rows)
            {
                row.Cells[1].Value = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var tempBs = new BindingSource();

            foreach (Item item in bsExtractedMails.List)
            {
                tempBs.Add(item);
            }

            var configuration = new ConfigurationScreen(tempBs, false)
            {
                Text = "Configure Mail Addresses",
                Label = "Current Mail Addresses:",
                AdditionText = "Add Mail Address:"
            };

            if (configuration.ShowDialog() == DialogResult.OK)
            {
                bsExtractedMails.Clear();
                foreach (Item item in tempBs.List)
                {
                    bsExtractedMails.Add(item);
                }

                MemoAssist.SetDataGridView(dataGridView1, bsExtractedMails);
            }
        }

        private void btnBodyAdd_Click(object sender, EventArgs e)
        {
            var newMemoBody = new AddMemoBody();

            if (newMemoBody.ShowDialog() == DialogResult.OK)
            {
                var memoBody = newMemoBody.GetMemoBody();

                if (!string.IsNullOrEmpty(memoBody))
                {
                    bsMemoBody.Add(new Item(memoBody, false, true, false));

                    MemoAssist.SetDataGridView(dataGridView2, bsMemoBody, true);
                }
            }
        }

        private void btnBodyDelete_Click(object sender, EventArgs e)
        {
            for (int i = dataGridView2.RowCount - 1; i >= 0; i--)
            {
                var row = dataGridView2.Rows[i];

                if ((bool)row.Cells[1].Value)
                {
                    bsMemoBody.RemoveAt(row.Index);
                }
            }
        }

        private async void btnPreview_Click(object sender, EventArgs e)
        {
            var previewMail = new PreviewEmail(GenerateModel());
            previewMail.ShowDialog();

            if (previewMail.Status == 1)
            {
                pnlStatus.Visible = true;

                await Task.Delay(3000);

                pnlStatus.Visible = false;
            }
            else if (previewMail.Status == 2)
            {
                lblMailStatusFailed.Visible = true;

                await Task.Delay(3000);

                lblMailStatusFailed.Visible = false;
            }
        }

        private async void btnSend_Click(object sender, EventArgs e)
        {
            var model = GenerateModel();
            var body = MemoAssist.Replacement(model.MemoBody, model);

            var mail = Globals.ThisAddIn.Application.CreateItem(Outlook.OlItemType.olMailItem) as Outlook.MailItem;
            mail.Subject = model.EmailSubject;
            mail.To = model.DeliveryTo;
            mail.CC = model.DeliveryCc;
            mail.Body = body;

            try
            {
                mail.Send();

                pnlStatus.Visible = true;

                await Task.Delay(3000);

                pnlStatus.Visible = false;
            }
            catch
            {
                lblMailStatusFailed.Visible = true;

                await Task.Delay(3000);

                lblMailStatusFailed.Visible = false;
            }
        }

        private Model GenerateModel()
        {
            var model = new Model();

            // Extracted Emails
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    if (string.IsNullOrEmpty(model.ExtractedMails))
                    {
                        model.ExtractedMails += (string)row.Cells[0].Value;
                    }
                    else
                    {
                        model.ExtractedMails += ", " + (string)row.Cells[0].Value;
                    }
                }
            }

            // Meeting Length
            foreach (DataGridViewRow row in dataGridView3.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    model.MeetingLength += (string)row.Cells[0].Value + " ";
                }
            }

            //Meeting Urgency
            foreach (DataGridViewRow row in dataGridView4.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    model.MeetingUrgency += (string)row.Cells[0].Value + " ";
                }
            }

            // Delivery To
            foreach (DataGridViewRow row in dataGridView5.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    model.DeliveryTo += (string)row.Cells[0].Value + ";";
                }
            }

            // Subject
            foreach (DataGridViewRow row in dataGridView6.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    model.EmailSubject += (string)row.Cells[0].Value + " ";
                }
            }

            // Memo Body
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    model.MemoBody = (string)row.Cells[0].Value;
                }
            }

            // Location
            foreach (DataGridViewRow row in dataGridView7.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    model.Location += (string)row.Cells[0].Value + " ";
                }
            }

            // Delivery Cc
            foreach (DataGridViewRow row in dataGridView8.Rows)
            {
                if ((bool)row.Cells[1].Value)
                {
                    model.DeliveryCc += (string)row.Cells[0].Value + ";";
                }
            }

            // Original Mail
            model.OriginalMail = mailBody;

            return model;
        }

        private PersistentModel GeneratePersistentModel()
        {
            var model = new PersistentModel
            {
                ExtractedMails = new List<string>(),
                MeetingLength = new List<string>(),
                MeetingUrgency = new List<string>(),
                DeliveryTo = new List<string>(),
                DeliveryCc = new List<string>(),
                EmailSubject = new List<string>(),
                MemoBody = new List<string>(),
                Location = new List<string>()
            };

            // Extracted Emails
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.ExtractedMails.Add((string)row.Cells[0].Value);
                }
            }

            // Meeting Length
            foreach (DataGridViewRow row in dataGridView3.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.MeetingLength.Add((string)row.Cells[0].Value);
                }
            }

            //Meeting Urgency
            foreach (DataGridViewRow row in dataGridView4.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.MeetingUrgency.Add((string)row.Cells[0].Value);
                }
            }

            // Delivery To
            foreach (DataGridViewRow row in dataGridView5.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.DeliveryTo.Add((string)row.Cells[0].Value);
                }
            }

            // Subject
            foreach (DataGridViewRow row in dataGridView6.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.EmailSubject.Add((string)row.Cells[0].Value);
                }
            }

            // Memo Body
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.MemoBody.Add((string)row.Cells[0].Value);
                }
            }

            // Location
            foreach (DataGridViewRow row in dataGridView7.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.Location.Add((string)row.Cells[0].Value);
                }
            }

            // Delivery Cc
            foreach (DataGridViewRow row in dataGridView8.Rows)
            {
                if ((bool)row.Cells[2].Value)
                {
                    model.DeliveryCc.Add((string)row.Cells[0].Value);
                }
            }

            return model;
        }

        // Length Of Meeting
        private BindingSource bsLengthOfMeeting = new BindingSource();

        private void InitLengthOfMeeting()
        {
            foreach (var mail in persistenceModel.MeetingLength)
            {
                bsLengthOfMeeting.Add(new Item(mail, false, true, false));
            }

            MemoAssist.SetDataGridView(dataGridView3, bsLengthOfMeeting);
        }

        private void btnConfigureMeetingLength_Click(object sender, EventArgs e)
        {
            var tempBs = new BindingSource();

            foreach (Item item in bsLengthOfMeeting.List)
            {
                tempBs.Add(item);
            }

            var configuration = new ConfigurationScreen(tempBs, false)
            {
                Text = "Configure Meeting Timeframes",
                Label = "Current Meeting Timeframes:",
                AdditionText = "Add Meeting Timeframe:"
            };

            if (configuration.ShowDialog() == DialogResult.OK)
            {
                bsLengthOfMeeting.Clear();
                foreach (Item item in tempBs.List)
                {
                    bsLengthOfMeeting.Add(item);
                }

                MemoAssist.SetDataGridView(dataGridView3, bsLengthOfMeeting);
            }
        }

        // Meeting Urgency
        private BindingSource bsMeetingUrgency = new BindingSource();

        private void InitMeetingUrgency()
        {
            foreach (var mail in persistenceModel.MeetingUrgency)
            {
                bsMeetingUrgency.Add(new Item(mail, false, true, false));
            }

            MemoAssist.SetDataGridView(dataGridView4, bsMeetingUrgency);
        }

        private void btnConfigureMeetingUrgency_Click(object sender, EventArgs e)
        {
            var tempBs = new BindingSource();

            foreach (Item item in bsMeetingUrgency.List)
            {
                tempBs.Add(item);
            }

            var configuration = new ConfigurationScreen(tempBs, false)
            {
                Text = "Configure Meeting Urgency",
                Label = "Current Meeting Urgencies:",
                AdditionText = "Add Meeting Urgency:"
            };

            if (configuration.ShowDialog() == DialogResult.OK)
            {
                bsMeetingUrgency.Clear();
                foreach (Item item in tempBs.List)
                {
                    bsMeetingUrgency.Add(item);
                }

                MemoAssist.SetDataGridView(dataGridView4, bsMeetingUrgency);
            }
        }

        // Delivery to
        private BindingSource bsDeliveryTo = new BindingSource();

        private void InitDeliveryTo()
        {
            foreach (var mail in extractedMails)
            {
                bsDeliveryTo.Add(new Item(mail, false, false, false));
            }
            foreach (var mail in persistenceModel.DeliveryTo)
            {
                bsDeliveryTo.Add(new Item(mail, false, true, false));
            }

            MemoAssist.SetDataGridView(dataGridView5, bsDeliveryTo);
        }

        private void btnConfigureDeliveryTo_Click(object sender, EventArgs e)
        {
            var tempBs = new BindingSource();

            foreach (Item item in bsDeliveryTo.List)
            {
                tempBs.Add(item);
            }

            var configuration = new ConfigurationScreen(tempBs, true)
            {
                Text = "Configure Delivery Addresses",
                Label = "Current Delivery Addresses:"
            };

            if (configuration.ShowDialog() == DialogResult.OK)
            {
                bsDeliveryTo.Clear();
                foreach (Item item in tempBs.List)
                {
                    bsDeliveryTo.Add(item);
                }

                MemoAssist.SetDataGridView(dataGridView5, bsDeliveryTo);
            }
        }

        // Subject
        private BindingSource bsSubject = new BindingSource();

        private void InitSubject()
        {
            foreach (var mail in subjects)
            {
                bsSubject.Add(new Item(mail, false, false, false));
            }
            foreach (var mail in persistenceModel.EmailSubject)
            {
                bsSubject.Add(new Item(mail, false, true, false));
            }

            MemoAssist.SetDataGridView(dataGridView6, bsSubject);
        }

        private void btnOriginal_Click(object sender, EventArgs e)
        {

        }

        private void btnConfigureSubject_Click(object sender, EventArgs e)
        {
            var tempBs = new BindingSource();

            foreach (Item item in bsSubject.List)
            {
                tempBs.Add(item);
            }

            var configuration = new ConfigurationScreen(tempBs, false)
            {
                Text = "Configure Mail Subjects",
                Label = "Current Mail Subjects:",
                AdditionText = "Add Mail Subject:"
            };

            if (configuration.ShowDialog() == DialogResult.OK)
            {
                bsSubject.Clear();
                foreach (Item item in tempBs.List)
                {
                    bsSubject.Add(item);
                }

                MemoAssist.SetDataGridView(dataGridView6, bsSubject);
            }
        }

        // Location
        private BindingSource bsLocation = new BindingSource();

        private void InitLocation()
        {
            var rooms = MemoAssist.GetAllRooms();

            foreach(var room in rooms)
            {
                bsLocation.Add(new Item(room, false, false, false));
            }
            foreach (var mail in persistenceModel.Location)
            {
                bsLocation.Add(new Item(mail, false, true, false));
            }

            MemoAssist.SetDataGridView(dataGridView7, bsLocation);
        }

        private void btnConfigureLocation_Click(object sender, EventArgs e)
        {
            var tempBs = new BindingSource();

            foreach (Item item in bsLocation.List)
            {
                tempBs.Add(item);
            }

            var configuration = new ConfigurationScreen(tempBs, false)
            {
                Text = "Configure Location",
                Label = "Current Locations:",
                AdditionText = "Add Location:"
            };

            if (configuration.ShowDialog() == DialogResult.OK)
            {
                bsLocation.Clear();
                foreach (Item item in tempBs.List)
                {
                    bsLocation.Add(item);
                }

                MemoAssist.SetDataGridView(dataGridView7, bsLocation);
            }
        }

        // Delivery Cc

        private BindingSource bsDeliveryCc = new BindingSource();

        private void InitDeliveryCc()
        {
            foreach (var mail in extractedMails)
            {
                bsDeliveryCc.Add(new Item(mail, false, false, false));
            }
            foreach (var mail in persistenceModel.DeliveryCc)
            {
                bsDeliveryCc.Add(new Item(mail, false, true, false));
            }

            MemoAssist.SetDataGridView(dataGridView8, bsDeliveryCc);
        }

        private void btnConfigureDeliveryCc_Click(object sender, EventArgs e)
        {
            var tempBs = new BindingSource();

            foreach (Item item in bsDeliveryCc.List)
            {
                tempBs.Add(item);
            }

            var configuration = new ConfigurationScreen(tempBs, true)
            {
                Text = "Configure Delivery Cc Addresses",
                Label = "Current Delivery Cc Addresses:"
            };

            if (configuration.ShowDialog() == DialogResult.OK)
            {
                bsDeliveryCc.Clear();
                foreach (Item item in tempBs.List)
                {
                    bsDeliveryCc.Add(item);
                }

                MemoAssist.SetDataGridView(dataGridView8, bsDeliveryCc);
            }
        }

        // Edit Memo Body
        private void btnBodyEdit_Click(object sender, EventArgs e)
        {
            foreach (Item item in bsMemoBody.List)
            {
                if (item.IsChecked)
                {
                    var editForm = new AddMemoBody(item.ItemName);

                    if (editForm.ShowDialog() == DialogResult.OK)
                    {
                        item.ItemName = editForm.GetMemoBody();
                    }

                    MemoAssist.SetDataGridView(dataGridView2, bsMemoBody);
                    dataGridView2.Refresh();

                    break;
                }
            }
        }

        private void MemoAssistant_FormClosing(object sender, FormClosingEventArgs e)
        {
            MemoAssist.Serialize(GeneratePersistentModel());
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            var help = new Help();
            help.ShowDialog();
        }
    }
}
