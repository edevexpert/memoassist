﻿using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MemoAssist.Forms
{
    public partial class PreviewEmail : Form
    {
        private Model model;

        public int Status { get; set; }

        public PreviewEmail(Model model)
        {
            InitializeComponent();

            this.model = model;

            Init();
        }

        private void Init()
        {
            if (!string.IsNullOrEmpty(model.DeliveryTo))
            {
                lblToEdit.Text = model.DeliveryTo;
            }
            else
            {
                lblToEdit.ForeColor = System.Drawing.Color.Red;
                lblToEdit.Text = "No Recipient specified!";
            }

            if (!string.IsNullOrEmpty(model.DeliveryCc))
            {
                lblCcEdit.Text = model.DeliveryCc;
            }
            else
            {
                lblCcEdit.ForeColor = System.Drawing.Color.Red;
                lblCcEdit.Text = "No Recipient in Cc specified!";
            }

            if (!string.IsNullOrEmpty(model.EmailSubject))
            {
                lblSubjectEdit.Text = model.EmailSubject;
            }
            else
            {
                lblSubjectEdit.ForeColor = System.Drawing.Color.Red;
                lblSubjectEdit.Text = "No Subject specified!";
            }

            txtPreviewMail.Text = MemoAssist.Replacement(model.MemoBody, model);
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnSend_Click(object sender, System.EventArgs e)
        {
            var mail = Globals.ThisAddIn.Application.CreateItem(Outlook.OlItemType.olMailItem) as Outlook.MailItem;
            mail.Subject = model.EmailSubject;
            mail.To = model.DeliveryTo;
            mail.CC = model.DeliveryCc;
            mail.Body = txtPreviewMail.Text;

            try
            {
                mail.Send();
                Status = 1;
            }
            catch { Status = 2; }

            Close();
        }
    }
}
