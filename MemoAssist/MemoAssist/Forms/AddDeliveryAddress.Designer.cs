﻿namespace MemoAssist.Forms
{
    partial class AddDeliveryAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNewDeliveryAddress = new System.Windows.Forms.Label();
            this.lblShortFormName = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtShortform = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNewDeliveryAddress
            // 
            this.lblNewDeliveryAddress.AutoSize = true;
            this.lblNewDeliveryAddress.Location = new System.Drawing.Point(10, 11);
            this.lblNewDeliveryAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNewDeliveryAddress.Name = "lblNewDeliveryAddress";
            this.lblNewDeliveryAddress.Size = new System.Drawing.Size(136, 13);
            this.lblNewDeliveryAddress.TabIndex = 0;
            this.lblNewDeliveryAddress.Text = "Add New Delivery Address:";
            // 
            // lblShortFormName
            // 
            this.lblShortFormName.AutoSize = true;
            this.lblShortFormName.Location = new System.Drawing.Point(10, 56);
            this.lblShortFormName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblShortFormName.Name = "lblShortFormName";
            this.lblShortFormName.Size = new System.Drawing.Size(108, 13);
            this.lblShortFormName.TabIndex = 1;
            this.lblShortFormName.Text = "Add Shortform Name:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(12, 28);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(255, 20);
            this.txtAddress.TabIndex = 2;
            // 
            // txtShortform
            // 
            this.txtShortform.Location = new System.Drawing.Point(12, 73);
            this.txtShortform.Margin = new System.Windows.Forms.Padding(2);
            this.txtShortform.Name = "txtShortform";
            this.txtShortform.Size = new System.Drawing.Size(255, 20);
            this.txtShortform.TabIndex = 3;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAdd.Location = new System.Drawing.Point(12, 95);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(89, 95);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // AddDeliveryAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 127);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtShortform);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lblShortFormName);
            this.Controls.Add(this.lblNewDeliveryAddress);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddDeliveryAddress";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNewDeliveryAddress;
        private System.Windows.Forms.Label lblShortFormName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtShortform;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
    }
}