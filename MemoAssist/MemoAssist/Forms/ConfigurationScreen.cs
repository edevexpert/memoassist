﻿using System;
using System.Windows.Forms;

namespace MemoAssist.Forms
{
    public partial class ConfigurationScreen : Form
    {
        private bool isDelivery = false;
        private BindingSource bs = default(BindingSource);

        public string Label
        {
            set
            {
                lblHeaderOne.Text = value;
            }
        }

        public string AdditionText { get; set; }

        public ConfigurationScreen(BindingSource bs, bool isDelivery)
        {
            InitializeComponent();

            this.bs = bs;
            this.isDelivery = isDelivery;

            MemoAssist.SetDataGridView(dataGridView1, bs, false, true);


            btnOK.DialogResult = DialogResult.OK;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (isDelivery)
            {
                var deliveryAddress = new AddDeliveryAddress();

                if (deliveryAddress.ShowDialog() == DialogResult.OK)
                {
                    var address = deliveryAddress.Address + (!string.IsNullOrEmpty(deliveryAddress.ShortForm) ? " (" + deliveryAddress.ShortForm + ")" : string.Empty);

                    if (!string.IsNullOrEmpty(address))
                    {
                        MemoAssist.SetDataGridViewItem(address, dataGridView1, bs, false, true);
                    }
                }
            }
            else
            {
                var additional = new AdditionalItem
                {
                    Label = AdditionText
                };

                if (additional.ShowDialog() == DialogResult.OK)
                {
                    if (!string.IsNullOrEmpty(additional.Additional))
                    {
                        MemoAssist.SetDataGridViewItem(additional.Additional, dataGridView1, bs, false, true);
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            for(int i = dataGridView1.RowCount - 1; i >= 0; i--)
            {
                var row = dataGridView1.Rows[i];

                if ((bool)row.Cells[3].Value)
                {
                    bs.RemoveAt(row.Index);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
