﻿using System.Windows.Forms;

namespace MemoAssist.Forms
{
    public partial class AddMemoBody : Form
    {
        public AddMemoBody(string text = "")
        {
            InitializeComponent();

            txtMemoBody.Text = text;

            btnDone.DialogResult = DialogResult.OK;
        }

        public string GetMemoBody()
        {
            return txtMemoBody.Text;
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnExtractedEmail_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Extracted Emails>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }

        private void btnMeetingLength_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Meeting Length>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }

        private void btnUrgency_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Meeting Urgency>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }

        private void btnDeliveryTo_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Delivery To>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }

        private void btnDeliveryCc_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Delivery Cc>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }

        private void btnSubject_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Email Subject>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }

        private void btnOriginalMail_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Original Mail>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }

        private void btnLocation_Click(object sender, System.EventArgs e)
        {
            var insertText = "<<Meeting Location>>";
            var selectionIndex = txtMemoBody.SelectionStart;
            txtMemoBody.Text = txtMemoBody.Text.Insert(selectionIndex, insertText);
            txtMemoBody.SelectionStart = selectionIndex + insertText.Length;

            txtMemoBody.Focus();
        }
    }
}
