﻿namespace MemoAssist.Forms
{
    partial class AddMemoBody
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.txtMemoBody = new System.Windows.Forms.TextBox();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExtractedEmail = new System.Windows.Forms.Button();
            this.btnMeetingLength = new System.Windows.Forms.Button();
            this.btnUrgency = new System.Windows.Forms.Button();
            this.btnDeliveryTo = new System.Windows.Forms.Button();
            this.btnSubject = new System.Windows.Forms.Button();
            this.btnOriginalMail = new System.Windows.Forms.Button();
            this.btnLocation = new System.Windows.Forms.Button();
            this.btnDeliveryCc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(10, 11);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(332, 13);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "Add New Memo Body - Insert Fields With Buttons (1000 Words Max):";
            // 
            // txtMemoBody
            // 
            this.txtMemoBody.Location = new System.Drawing.Point(12, 28);
            this.txtMemoBody.Margin = new System.Windows.Forms.Padding(2);
            this.txtMemoBody.Multiline = true;
            this.txtMemoBody.Name = "txtMemoBody";
            this.txtMemoBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMemoBody.Size = new System.Drawing.Size(356, 288);
            this.txtMemoBody.TabIndex = 1;
            // 
            // btnDone
            // 
            this.btnDone.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDone.FlatAppearance.BorderSize = 0;
            this.btnDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDone.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDone.Location = new System.Drawing.Point(12, 318);
            this.btnDone.Margin = new System.Windows.Forms.Padding(2);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 2;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(89, 318);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnExtractedEmail
            // 
            this.btnExtractedEmail.BackColor = System.Drawing.Color.LimeGreen;
            this.btnExtractedEmail.FlatAppearance.BorderSize = 0;
            this.btnExtractedEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExtractedEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExtractedEmail.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnExtractedEmail.Location = new System.Drawing.Point(370, 28);
            this.btnExtractedEmail.Margin = new System.Windows.Forms.Padding(2);
            this.btnExtractedEmail.Name = "btnExtractedEmail";
            this.btnExtractedEmail.Size = new System.Drawing.Size(113, 23);
            this.btnExtractedEmail.TabIndex = 4;
            this.btnExtractedEmail.Text = "Extracted Email";
            this.btnExtractedEmail.UseVisualStyleBackColor = false;
            this.btnExtractedEmail.Click += new System.EventHandler(this.btnExtractedEmail_Click);
            // 
            // btnMeetingLength
            // 
            this.btnMeetingLength.BackColor = System.Drawing.Color.LimeGreen;
            this.btnMeetingLength.FlatAppearance.BorderSize = 0;
            this.btnMeetingLength.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMeetingLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeetingLength.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnMeetingLength.Location = new System.Drawing.Point(370, 53);
            this.btnMeetingLength.Margin = new System.Windows.Forms.Padding(2);
            this.btnMeetingLength.Name = "btnMeetingLength";
            this.btnMeetingLength.Size = new System.Drawing.Size(113, 23);
            this.btnMeetingLength.TabIndex = 5;
            this.btnMeetingLength.Text = "Meeting Length";
            this.btnMeetingLength.UseVisualStyleBackColor = false;
            this.btnMeetingLength.Click += new System.EventHandler(this.btnMeetingLength_Click);
            // 
            // btnUrgency
            // 
            this.btnUrgency.BackColor = System.Drawing.Color.LimeGreen;
            this.btnUrgency.FlatAppearance.BorderSize = 0;
            this.btnUrgency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUrgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUrgency.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnUrgency.Location = new System.Drawing.Point(370, 78);
            this.btnUrgency.Margin = new System.Windows.Forms.Padding(2);
            this.btnUrgency.Name = "btnUrgency";
            this.btnUrgency.Size = new System.Drawing.Size(113, 23);
            this.btnUrgency.TabIndex = 6;
            this.btnUrgency.Text = "Meeting Urgency";
            this.btnUrgency.UseVisualStyleBackColor = false;
            this.btnUrgency.Click += new System.EventHandler(this.btnUrgency_Click);
            // 
            // btnDeliveryTo
            // 
            this.btnDeliveryTo.BackColor = System.Drawing.Color.LimeGreen;
            this.btnDeliveryTo.FlatAppearance.BorderSize = 0;
            this.btnDeliveryTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeliveryTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeliveryTo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDeliveryTo.Location = new System.Drawing.Point(370, 103);
            this.btnDeliveryTo.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeliveryTo.Name = "btnDeliveryTo";
            this.btnDeliveryTo.Size = new System.Drawing.Size(113, 23);
            this.btnDeliveryTo.TabIndex = 7;
            this.btnDeliveryTo.Text = "Delivery To";
            this.btnDeliveryTo.UseVisualStyleBackColor = false;
            this.btnDeliveryTo.Click += new System.EventHandler(this.btnDeliveryTo_Click);
            // 
            // btnSubject
            // 
            this.btnSubject.BackColor = System.Drawing.Color.LimeGreen;
            this.btnSubject.FlatAppearance.BorderSize = 0;
            this.btnSubject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubject.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSubject.Location = new System.Drawing.Point(370, 153);
            this.btnSubject.Margin = new System.Windows.Forms.Padding(2);
            this.btnSubject.Name = "btnSubject";
            this.btnSubject.Size = new System.Drawing.Size(113, 23);
            this.btnSubject.TabIndex = 9;
            this.btnSubject.Text = "Email Subject";
            this.btnSubject.UseVisualStyleBackColor = false;
            this.btnSubject.Click += new System.EventHandler(this.btnSubject_Click);
            // 
            // btnOriginalMail
            // 
            this.btnOriginalMail.BackColor = System.Drawing.Color.LimeGreen;
            this.btnOriginalMail.FlatAppearance.BorderSize = 0;
            this.btnOriginalMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOriginalMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOriginalMail.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOriginalMail.Location = new System.Drawing.Point(370, 203);
            this.btnOriginalMail.Margin = new System.Windows.Forms.Padding(2);
            this.btnOriginalMail.Name = "btnOriginalMail";
            this.btnOriginalMail.Size = new System.Drawing.Size(113, 23);
            this.btnOriginalMail.TabIndex = 10;
            this.btnOriginalMail.Text = "Original Mail";
            this.btnOriginalMail.UseVisualStyleBackColor = false;
            this.btnOriginalMail.Click += new System.EventHandler(this.btnOriginalMail_Click);
            // 
            // btnLocation
            // 
            this.btnLocation.BackColor = System.Drawing.Color.LimeGreen;
            this.btnLocation.FlatAppearance.BorderSize = 0;
            this.btnLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocation.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLocation.Location = new System.Drawing.Point(370, 178);
            this.btnLocation.Margin = new System.Windows.Forms.Padding(2);
            this.btnLocation.Name = "btnLocation";
            this.btnLocation.Size = new System.Drawing.Size(113, 23);
            this.btnLocation.TabIndex = 11;
            this.btnLocation.Text = "Meeting Location";
            this.btnLocation.UseVisualStyleBackColor = false;
            this.btnLocation.Click += new System.EventHandler(this.btnLocation_Click);
            // 
            // btnDeliveryCc
            // 
            this.btnDeliveryCc.BackColor = System.Drawing.Color.LimeGreen;
            this.btnDeliveryCc.FlatAppearance.BorderSize = 0;
            this.btnDeliveryCc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeliveryCc.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeliveryCc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDeliveryCc.Location = new System.Drawing.Point(370, 128);
            this.btnDeliveryCc.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeliveryCc.Name = "btnDeliveryCc";
            this.btnDeliveryCc.Size = new System.Drawing.Size(113, 23);
            this.btnDeliveryCc.TabIndex = 12;
            this.btnDeliveryCc.Text = "Delivery Cc";
            this.btnDeliveryCc.UseVisualStyleBackColor = false;
            this.btnDeliveryCc.Click += new System.EventHandler(this.btnDeliveryCc_Click);
            // 
            // AddMemoBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 350);
            this.Controls.Add(this.btnDeliveryCc);
            this.Controls.Add(this.btnLocation);
            this.Controls.Add(this.btnOriginalMail);
            this.Controls.Add(this.btnSubject);
            this.Controls.Add(this.btnDeliveryTo);
            this.Controls.Add(this.btnUrgency);
            this.Controls.Add(this.btnMeetingLength);
            this.Controls.Add(this.btnExtractedEmail);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.txtMemoBody);
            this.Controls.Add(this.lblHeader);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddMemoBody";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Memo Body";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.TextBox txtMemoBody;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnExtractedEmail;
        private System.Windows.Forms.Button btnMeetingLength;
        private System.Windows.Forms.Button btnUrgency;
        private System.Windows.Forms.Button btnDeliveryTo;
        private System.Windows.Forms.Button btnSubject;
        private System.Windows.Forms.Button btnOriginalMail;
        private System.Windows.Forms.Button btnLocation;
        private System.Windows.Forms.Button btnDeliveryCc;
    }
}