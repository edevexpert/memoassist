﻿namespace MemoAssist.Forms
{
    partial class PreviewEmail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPreviewMail = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblToEdit = new System.Windows.Forms.Label();
            this.lblSubjectEdit = new System.Windows.Forms.Label();
            this.lblCc = new System.Windows.Forms.Label();
            this.lblCcEdit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPreviewMail
            // 
            this.txtPreviewMail.Location = new System.Drawing.Point(9, 77);
            this.txtPreviewMail.Margin = new System.Windows.Forms.Padding(2);
            this.txtPreviewMail.Multiline = true;
            this.txtPreviewMail.Name = "txtPreviewMail";
            this.txtPreviewMail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPreviewMail.Size = new System.Drawing.Size(414, 392);
            this.txtPreviewMail.TabIndex = 0;
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.Red;
            this.btnSend.FlatAppearance.BorderSize = 0;
            this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSend.Location = new System.Drawing.Point(9, 471);
            this.btnSend.Margin = new System.Windows.Forms.Padding(2);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 1;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCancel.Location = new System.Drawing.Point(86, 471);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(9, 13);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(25, 13);
            this.lblTo.TabIndex = 3;
            this.lblTo.Text = "TO:";
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(9, 55);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(49, 13);
            this.lblSubject.TabIndex = 4;
            this.lblSubject.Text = "Subject: ";
            // 
            // lblToEdit
            // 
            this.lblToEdit.AutoSize = true;
            this.lblToEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToEdit.Location = new System.Drawing.Point(41, 13);
            this.lblToEdit.Name = "lblToEdit";
            this.lblToEdit.Size = new System.Drawing.Size(19, 13);
            this.lblToEdit.TabIndex = 5;
            this.lblToEdit.Text = "---";
            // 
            // lblSubjectEdit
            // 
            this.lblSubjectEdit.AutoSize = true;
            this.lblSubjectEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubjectEdit.Location = new System.Drawing.Point(62, 55);
            this.lblSubjectEdit.Name = "lblSubjectEdit";
            this.lblSubjectEdit.Size = new System.Drawing.Size(19, 13);
            this.lblSubjectEdit.TabIndex = 6;
            this.lblSubjectEdit.Text = "---";
            // 
            // lblCc
            // 
            this.lblCc.AutoSize = true;
            this.lblCc.Location = new System.Drawing.Point(9, 34);
            this.lblCc.Name = "lblCc";
            this.lblCc.Size = new System.Drawing.Size(24, 13);
            this.lblCc.TabIndex = 7;
            this.lblCc.Text = "CC:";
            // 
            // lblCcEdit
            // 
            this.lblCcEdit.AutoSize = true;
            this.lblCcEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCcEdit.Location = new System.Drawing.Point(41, 34);
            this.lblCcEdit.Name = "lblCcEdit";
            this.lblCcEdit.Size = new System.Drawing.Size(19, 13);
            this.lblCcEdit.TabIndex = 8;
            this.lblCcEdit.Text = "---";
            // 
            // PreviewEmail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 503);
            this.Controls.Add(this.lblCcEdit);
            this.Controls.Add(this.lblCc);
            this.Controls.Add(this.lblSubjectEdit);
            this.Controls.Add(this.lblToEdit);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtPreviewMail);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PreviewEmail";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Preview Email";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPreviewMail;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblToEdit;
        private System.Windows.Forms.Label lblSubjectEdit;
        private System.Windows.Forms.Label lblCc;
        private System.Windows.Forms.Label lblCcEdit;
    }
}