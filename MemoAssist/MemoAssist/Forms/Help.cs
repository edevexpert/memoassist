﻿using System;
using System.Windows.Forms;

namespace MemoAssist.Forms
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lbl1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "Adding An Additional Extracted Email";

            rtbText.Clear();
            rtbText.AppendText("Extracted emails are addresses that were included in the original email to you. These will be emails that will be included in the body of your email. (Address to actually send the memo to are handled under the Memo Template Delivery To)" + Environment.NewLine + Environment.NewLine +
                                "In certain circumstances, such as special email templates, you may want to add additional emails to this list.To do so, click on the ADD button under the Extracted Email panel, enter the email address and then click DONE." + Environment.NewLine + Environment.NewLine +
                                "The address will be added to the overall email selection. You can then select individual emails to be included in your email template body by clicking on the boxes next to the addresses or click SELECT ALL for all the addresses to be added.");
        }

        private void lbl2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "Managing Meeting Lengths";

            rtbText.Clear();
            rtbText.AppendText("This is where you’ll specify how long you’d like this meeting to last. If one of the default times does not work for you click on the CONFIGURE button under the table and add the additional time frame. You can then select it from the list using the selection box next to it.");
        }

        private void lbl3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "Setting the Urgency of a Meeting";

            rtbText.Clear();
            rtbText.AppendText("Here is where you set the timeframe of when you want the meeting to occur. It’s usually best to give the largest timeframe possible to help accommodate everyone’s schedules that the meeting request will need to go out to.  If you wish to add additional date parameters click on the CONFIGURE button below the table and add the new timeframe. You can then select it from the list.");
        }

        private void lbl4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "Configuring Template Delivery Addresses";

            rtbText.Clear();
            rtbText.AppendText("While the tool extracts all the email addresses associated with the original email.  More often than not you likely will want the memo to go to an unrelated address such as your assistant or secretary. Click on the CONFIGURE button under the table and add the address of the individual you want processing the memo in to a meeting request. You can then select it from the list using the selection box next to it.");
        }

        private void lbl5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "Configuring Subjects";

            rtbText.Clear();
            rtbText.AppendText("The original subject line from the email is already populated there for your selection, but you may want to create certain subject lines that communicate additional instructions to the receiver or trigger automatic rule processes on the receivers email client. Click on the CONFIGURE button to add additional subject lines. You can then select the subject you want the memo to contain. Selecting the ORIGINAL button simply selects the subject that was extracted from the original email.");
        }

        private void lbl6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "Creating the Message Body and Configuration";

            rtbText.Clear();
            rtbText.AppendText("Here is where you will create actual memo templates that fit your various meeting creation needs. Click on the ADD button to open the MEMO BODY creation box. Begin typing in the white space whatever message you want the recipient to abide by. (EX: “Please create a meeting that invites…”) " + Environment.NewLine + Environment.NewLine +
                                "Along the right side of the body area there are extracted field buttons.Click on these buttons to insert that particular field within the body of the memo.For example clicking on “Extracted Emails” Inserts the << Extracted Emails >> field in to the message body.The selected emails in the Extracted Emails table will then be listed at that point in the memo." + Environment.NewLine + Environment.NewLine +
                                "When you are finished crafting the memo, click Done." + Environment.NewLine + Environment.NewLine +
                                "You can then select the memo by clicking on the check box next to it." + Environment.NewLine + Environment.NewLine +
                                "To view the memo before it goes out, click PREVIEW EMAIL." + Environment.NewLine + Environment.NewLine +
                                "You can then click SEND EMAIL to send the email to the selected addresses from the Memo Template Delivery To table.");
        }

        private void lbl7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "About Us";

            rtbText.Clear();
            rtbText.AppendText("ipCapital Group (ipCG) is an intellectual property (IP) and innovation consulting firm, since 1998." + Environment.NewLine + Environment.NewLine +
                                "ipCG has delivered over 800 successful innovation and IP engagements, including to over 10 % of the Fortune 500, in a wide range of industries." + Environment.NewLine + Environment.NewLine +
                                "Our interdisciplinary team of consultants and advisors are focused on maximizing clients’ financial results.We combine our world -class human capital with our proprietary tools and methodologies around innovation, invention, and IP." + Environment.NewLine + Environment.NewLine +
                                "To best achieve their business objectives, we are flexible in how we partner with clients: " + Environment.NewLine + Environment.NewLine +
                                " - Management consulting" + Environment.NewLine + Environment.NewLine +
                                " - Innovation & invention programs" + Environment.NewLine + Environment.NewLine +
                                " - Monetization strategy & execution" + Environment.NewLine + Environment.NewLine +
                                " - Software for managing IP & innovation");
        }

        private void lbl8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pctBusinessMan.Visible = false;

            lblHeader.Text = "Additional Support Email";

            rtbText.Clear();
            rtbText.AppendText("For additional support questions please email support@ipcg.com");
        }
    }
}
