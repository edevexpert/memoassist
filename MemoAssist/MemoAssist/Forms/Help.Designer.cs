﻿namespace MemoAssist.Forms
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help));
            this.pnlHelp = new System.Windows.Forms.Panel();
            this.lblMemoAssistant = new System.Windows.Forms.Label();
            this.pctBox = new System.Windows.Forms.PictureBox();
            this.lblHelpTopics = new System.Windows.Forms.Label();
            this.btnDone = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pctBusinessMan = new System.Windows.Forms.PictureBox();
            this.rtbText = new System.Windows.Forms.RichTextBox();
            this.lblHeader = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.LinkLabel();
            this.lbl2 = new System.Windows.Forms.LinkLabel();
            this.lbl3 = new System.Windows.Forms.LinkLabel();
            this.lbl4 = new System.Windows.Forms.LinkLabel();
            this.lbl5 = new System.Windows.Forms.LinkLabel();
            this.lbl6 = new System.Windows.Forms.LinkLabel();
            this.lbl7 = new System.Windows.Forms.LinkLabel();
            this.lbl8 = new System.Windows.Forms.LinkLabel();
            this.pnlHelp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBusinessMan)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHelp
            // 
            this.pnlHelp.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlHelp.Controls.Add(this.lblMemoAssistant);
            this.pnlHelp.Controls.Add(this.pctBox);
            this.pnlHelp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pnlHelp.Location = new System.Drawing.Point(51, 57);
            this.pnlHelp.Margin = new System.Windows.Forms.Padding(0);
            this.pnlHelp.Name = "pnlHelp";
            this.pnlHelp.Size = new System.Drawing.Size(275, 138);
            this.pnlHelp.TabIndex = 10;
            // 
            // lblMemoAssistant
            // 
            this.lblMemoAssistant.AutoSize = true;
            this.lblMemoAssistant.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemoAssistant.Location = new System.Drawing.Point(127, 40);
            this.lblMemoAssistant.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMemoAssistant.Name = "lblMemoAssistant";
            this.lblMemoAssistant.Size = new System.Drawing.Size(117, 58);
            this.lblMemoAssistant.TabIndex = 2;
            this.lblMemoAssistant.Text = "Memo \r\nAssistant";
            this.lblMemoAssistant.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pctBox
            // 
            this.pctBox.Image = ((System.Drawing.Image)(resources.GetObject("pctBox.Image")));
            this.pctBox.Location = new System.Drawing.Point(14, 17);
            this.pctBox.Margin = new System.Windows.Forms.Padding(0);
            this.pctBox.Name = "pctBox";
            this.pctBox.Size = new System.Drawing.Size(89, 106);
            this.pctBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBox.TabIndex = 1;
            this.pctBox.TabStop = false;
            // 
            // lblHelpTopics
            // 
            this.lblHelpTopics.AutoSize = true;
            this.lblHelpTopics.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHelpTopics.ForeColor = System.Drawing.Color.Red;
            this.lblHelpTopics.Location = new System.Drawing.Point(90, 210);
            this.lblHelpTopics.Name = "lblHelpTopics";
            this.lblHelpTopics.Size = new System.Drawing.Size(205, 31);
            this.lblHelpTopics.TabIndex = 11;
            this.lblHelpTopics.Text = "HELP TOPICS";
            // 
            // btnDone
            // 
            this.btnDone.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDone.FlatAppearance.BorderSize = 0;
            this.btnDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDone.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDone.Location = new System.Drawing.Point(51, 661);
            this.btnDone.Margin = new System.Windows.Forms.Padding(2);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(171, 32);
            this.btnDone.TabIndex = 12;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = false;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pctBusinessMan);
            this.panel1.Controls.Add(this.rtbText);
            this.panel1.Controls.Add(this.lblHeader);
            this.panel1.Location = new System.Drawing.Point(372, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(731, 585);
            this.panel1.TabIndex = 13;
            // 
            // pctBusinessMan
            // 
            this.pctBusinessMan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pctBusinessMan.Image = ((System.Drawing.Image)(resources.GetObject("pctBusinessMan.Image")));
            this.pctBusinessMan.Location = new System.Drawing.Point(0, 0);
            this.pctBusinessMan.Name = "pctBusinessMan";
            this.pctBusinessMan.Size = new System.Drawing.Size(731, 585);
            this.pctBusinessMan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBusinessMan.TabIndex = 2;
            this.pctBusinessMan.TabStop = false;
            // 
            // rtbText
            // 
            this.rtbText.BackColor = System.Drawing.SystemColors.Control;
            this.rtbText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbText.Location = new System.Drawing.Point(185, 153);
            this.rtbText.Margin = new System.Windows.Forms.Padding(0);
            this.rtbText.Name = "rtbText";
            this.rtbText.ReadOnly = true;
            this.rtbText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtbText.Size = new System.Drawing.Size(391, 355);
            this.rtbText.TabIndex = 1;
            this.rtbText.Text = "";
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(181, 118);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(0, 20);
            this.lblHeader.TabIndex = 0;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(48, 277);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(240, 17);
            this.lbl1.TabIndex = 14;
            this.lbl1.TabStop = true;
            this.lbl1.Text = "Adding An Additional Extracted Email";
            this.lbl1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl1_LinkClicked);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(48, 309);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(179, 17);
            this.lbl2.TabIndex = 15;
            this.lbl2.TabStop = true;
            this.lbl2.Text = "Managing Meeting Lengths";
            this.lbl2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl2_LinkClicked);
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(48, 346);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(215, 17);
            this.lbl3.TabIndex = 16;
            this.lbl3.TabStop = true;
            this.lbl3.Text = "Setting the Urgency of a Meeting";
            this.lbl3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl3_LinkClicked);
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.Location = new System.Drawing.Point(48, 381);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(269, 17);
            this.lbl4.TabIndex = 17;
            this.lbl4.TabStop = true;
            this.lbl4.Text = "Configuring Template Delivery Addresses";
            this.lbl4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl4_LinkClicked);
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Location = new System.Drawing.Point(48, 421);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(138, 17);
            this.lbl5.TabIndex = 18;
            this.lbl5.TabStop = true;
            this.lbl5.Text = "Configuring Subjects";
            this.lbl5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl5_LinkClicked);
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6.Location = new System.Drawing.Point(48, 459);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(298, 17);
            this.lbl6.TabIndex = 19;
            this.lbl6.TabStop = true;
            this.lbl6.Text = "Creating the Message Body and Configuration";
            this.lbl6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl6_LinkClicked);
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7.Location = new System.Drawing.Point(48, 594);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(66, 17);
            this.lbl7.TabIndex = 20;
            this.lbl7.TabStop = true;
            this.lbl7.Text = "About Us";
            this.lbl7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl7_LinkClicked);
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.Location = new System.Drawing.Point(48, 625);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(162, 17);
            this.lbl8.TabIndex = 21;
            this.lbl8.TabStop = true;
            this.lbl8.Text = "Additional Support Email";
            this.lbl8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl8_LinkClicked);
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 729);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.lblHelpTopics);
            this.Controls.Add(this.pnlHelp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Help";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Help";
            this.pnlHelp.ResumeLayout(false);
            this.pnlHelp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBusinessMan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHelp;
        private System.Windows.Forms.Label lblMemoAssistant;
        private System.Windows.Forms.PictureBox pctBox;
        private System.Windows.Forms.Label lblHelpTopics;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel lbl1;
        private System.Windows.Forms.LinkLabel lbl2;
        private System.Windows.Forms.LinkLabel lbl3;
        private System.Windows.Forms.LinkLabel lbl4;
        private System.Windows.Forms.LinkLabel lbl5;
        private System.Windows.Forms.LinkLabel lbl6;
        private System.Windows.Forms.LinkLabel lbl7;
        private System.Windows.Forms.LinkLabel lbl8;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.RichTextBox rtbText;
        private System.Windows.Forms.PictureBox pctBusinessMan;
    }
}