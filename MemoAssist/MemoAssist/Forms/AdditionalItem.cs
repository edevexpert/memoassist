﻿using System.Windows.Forms;

namespace MemoAssist.Forms
{
    public partial class AdditionalItem : Form
    {
        public string Label
        {
            set
            {
                lblAdditional.Text = value;
            }
        }

        public string Additional
        {
            get
            {
                return txtAdditionalEmail.Text;
            }
        }

        public AdditionalItem()
        {
            InitializeComponent();

            btnAdd.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}
