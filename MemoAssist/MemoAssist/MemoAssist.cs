﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace MemoAssist
{
    public static class MemoAssist
    {
        #region Persistence

        private static string dataModelPersistance = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MemoAssist.txt");

        public static void Serialize(PersistentModel data)
        {
            using (var sw = new StreamWriter(dataModelPersistance))
            {
                sw.Write(JsonConvert.SerializeObject(data));
            }
        }

        public static PersistentModel Deserialize()
        {
            PersistentModel data = null;

            using (var dr = new StreamReader(dataModelPersistance))
            {
                data = JsonConvert.DeserializeObject<PersistentModel>(dr.ReadToEnd());
            }

            return data;
        }

        #endregion

        #region Extraction

        public static void ExtractMailAddresses(Outlook.MailItem mail, List<string> extractedMails)
        {
            ParseEmail(mail.Body, extractedMails);
            ParseEmail(mail.Subject, extractedMails);

            try
            {
                if (mail.Sender.Type.Equals("SMTP", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    var senderEmailAddress = mail.Sender.Address;
                    AddExMail(extractedMails, senderEmailAddress);
                }
                else if (mail.Sender.Type.Equals("Ex", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    var senderEmailAddress = mail.Sender.GetExchangeUser().PrimarySmtpAddress;
                    AddExMail(extractedMails, senderEmailAddress);
                }
            }
            catch { }


            foreach (Outlook.Recipient c in mail.Recipients)
            {
                if (c.AddressEntry.Type.Equals("Ex", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    try
                    {
                        var address = c.AddressEntry.GetExchangeUser().PrimarySmtpAddress;

                        if (!string.IsNullOrEmpty(address))
                        {
                            AddExMail(extractedMails, address);
                        }
                    }
                    catch { }
                }
                else if (c.AddressEntry.Type.Equals("SMTP", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    try
                    {
                        var address = c.Address;

                        if (!string.IsNullOrEmpty(address))
                        {
                            AddExMail(extractedMails, address);
                        }
                    }
                    catch { }
                }
            }
        }

        private static void ParseEmail(string mailBody, List<string> extractedMails)
        {
            const string MatchEmailPattern =
           @"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})";

            Regex rx = new Regex(MatchEmailPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            MatchCollection matches = rx.Matches(mailBody);
            foreach (Match match in matches)
            {
                AddExMail(extractedMails, match.Value.ToString());
            }
        }

        private static void AddExMail(List<string> extractedMails, string mail)
        {
            if (!extractedMails.Contains(mail))
            {
                extractedMails.Add(mail);
            }
        }

        public static string Replacement(string rawMail, Model model)
        {
            var sb = new StringBuilder(rawMail);

            sb.Replace("<<Extracted Emails>>", model.ExtractedMails);
            sb.Replace("<<Meeting Length>>", model.MeetingLength);
            sb.Replace("<<Meeting Urgency>>", model.MeetingUrgency);

            var deliveryTo = string.Empty;
            if (!string.IsNullOrEmpty(model.DeliveryTo))
            {
                var splitedText = model.DeliveryTo.Split(';');

                var re = new Regex(@"(?<=\().*(?=\))");

                foreach (var text in splitedText)
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        var matches = re.Matches(text);

                        if (matches.Count != 0)
                        {
                            foreach (Match match in matches)
                            {
                                var m = match.Groups[0];

                                if (string.IsNullOrEmpty(deliveryTo))
                                {
                                    deliveryTo += m.Value.ToString();
                                }
                                else
                                {
                                    deliveryTo += ", " + m.Value.ToString();
                                }
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(deliveryTo))
                            {
                                deliveryTo += text;
                            }
                            else
                            {
                                deliveryTo += ", " + text;
                            }
                        }
                    }
                }
            }

            var deliveryCc = string.Empty;
            if (!string.IsNullOrEmpty(model.DeliveryCc))
            {
                var splitedText = model.DeliveryCc.Split(';');

                var re = new Regex(@"(?<=\().*(?=\))");

                foreach (var text in splitedText)
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        var matches = re.Matches(text);

                        if (matches.Count != 0)
                        {
                            foreach (Match match in matches)
                            {
                                var m = match.Groups[0];

                                if (string.IsNullOrEmpty(deliveryCc))
                                {
                                    deliveryCc += m.Value.ToString();
                                }
                                else
                                {
                                    deliveryCc += ", " + m.Value.ToString();
                                }
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(deliveryCc))
                            {
                                deliveryCc += text;
                            }
                            else
                            {
                                deliveryCc += ", " + text;
                            }
                        }
                    }
                }
            }

            sb.Replace("<<Delivery To>>", deliveryTo);
            sb.Replace("<<Delivery Cc>>", deliveryCc);
            sb.Replace("<<Email Subject>>", model.EmailSubject);
            sb.Replace("<<Meeting Location>>", model.Location);
            sb.Replace("<<Original Mail>>", model.OriginalMail);

            return sb.ToString();
        }

        #endregion

        #region DataGridView

        public static void SetDataGridView(DataGridView dgv, BindingSource bs, bool breakLine = false, bool editMode = false)
        {
            if (bs.Count != 0)
            {
                dgv.DataSource = bs;

                if (editMode)
                {
                    dgv.Columns[1].Visible = false;
                    dgv.Columns[1].Width = 0;

                    dgv.Columns[3].Visible = true;
                    dgv.Columns[3].Width = 30;

                    dgv.Columns[0].Width = dgv.Width - dgv.Columns[1].Width - 45;
                }
                else
                {
                    dgv.Columns[1].Visible = true;
                    dgv.Columns[1].Width = 30;

                    dgv.Columns[3].Visible = false;
                    dgv.Columns[3].Width = 0;

                    dgv.Columns[0].Width = dgv.Width - dgv.Columns[1].Width - 25;
                }

                dgv.Columns[2].Visible = false;

                if (breakLine)
                {
                    dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
                }
            }
        }

        public static void SetDataGridViewItem(string itemName, DataGridView dgv, BindingSource bs, bool breakLine = false, bool editMode = false)
        {
            var reinitialize = bs.Count == 0;

            bs.Add(new Item(itemName, false, true, false));

            if (reinitialize)
            {
                SetDataGridView(dgv, bs, breakLine, editMode);
            }
        }

        #endregion

        #region Misc

        public static List<string> GetAllRooms()
        {
            var addresses = new List<string>();

            try
            {
                Outlook.AddressList room = null;
                var PR_CONTAINER_FLAGS = "http://schemas.microsoft.com/mapi/proptag/0x36000003";

                var lists = Globals.ThisAddIn.Application.Session.AddressLists;

                foreach (Outlook.AddressList list in lists)
                {
                    var containerFlags = list.PropertyAccessor.GetProperty(PR_CONTAINER_FLAGS);
                    if ((containerFlags & 0x00000200) != 0)
                    {
                        room = list;
                    }
                }

                if (room != null)
                {
                    foreach (Outlook.AddressEntry address in room.AddressEntries)
                    {
                        addresses.Add(address.Name);
                    }
                }
            }
            catch { }

            return addresses;
        }

        #endregion
    }
}
