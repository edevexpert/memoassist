﻿using System.Collections.Generic;

namespace MemoAssist
{
    public class Item
    {
        public string ItemName { get; set; }
        public bool IsChecked { get; set; }
        public bool IsPermanent { get; set; }
        public bool IsEditMode { get; set; }

        public Item(string itemName, bool isChecked, bool isPermanent, bool isEditMode)
        {
            ItemName = itemName;
            IsChecked = isChecked;
            IsPermanent = isPermanent;
            IsEditMode = isEditMode;
        }
    }

    public class Model
    {
        public string ExtractedMails { get; set; }
        public string MeetingLength { get; set; }
        public string MeetingUrgency { get; set; }
        public string DeliveryTo { get; set; }
        public string DeliveryCc { get; set; }
        public string EmailSubject { get; set; }
        public string Location { get; set; }
        public string MemoBody { get; set; }

        public string OriginalMail { get; set; }
    }

    public class PersistentModel
    {
        public List<string> ExtractedMails { get; set; }
        public List<string> MeetingLength { get; set; }
        public List<string> MeetingUrgency { get; set; }
        public List<string> DeliveryTo { get; set; }
        public List<string> DeliveryCc { get; set; }
        public List<string> EmailSubject { get; set; }
        public List<string> Location { get; set; }
        public List<string> MemoBody { get; set; }
    }
}
